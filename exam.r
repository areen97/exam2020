
install.packages('chron')
library(chron)

install.packages('dplyr')
library(dplyr)

install.packages('caTools')
library(caTools)

install.packages('rpart')
library(rpart)
library(rpart.plot)

setwd("C:/")

insurance.raw <- read.csv('carInsurance_train.csv')

str(insurance.raw )

insurance.raw$CallTime <- chron(times=insurance.raw$CallEnd) - chron(times=insurance.raw$CallStart)
insurance.raw$CallTimeSeconds <- hours(insurance.raw$CallTime)*60*60+minutes(insurance.raw$CallTime)*60+seconds(insurance.raw$CallTime)

insurance <- insurance.raw %>% select(CarInsurance, Age, Default, Education, Balance , PrevAttempts, CallTimeSeconds,  NoOfContacts, CarLoan,Job)

coercx <- function(x,by){
  if(x<=by) return(x)
  return(by)
}

# Q2
insurance$CarLoan <- as.factor(insurance$CarLoan)
insurance$Education <- as.factor(insurance$Education)
insurance$Job <- as.factor(insurance$Job)
insurance$Default <- as.factor(insurance$Default)

insurance$PrevAttempts <- sapply(insurance$PrevAttempts, coercx, by=10)
ggplot(insurance, aes(PrevAttempts, fill=CarInsurance)) + geom_histogram()

insurance$CallTimeSeconds <- sapply(insurance$CallTimeSeconds, coercx, by=1400)
ggplot(insurance, aes(CallTimeSeconds, fill=CarInsurance)) + geom_histogram()

insurance$Age <- sapply(insurance$Age, coercx, by=75)
ggplot(insurance, aes(Age, fill=CarInsurance)) + geom_histogram()

insurance$Balance <- sapply(insurance$Balance, coercx, by=12000)
ggplot(insurance, aes(Balance, fill=CarInsurance)) + geom_histogram()


filter <- sample.split(insurance$CarInsurance, SplitRatio=0.7)
insurance.test <- subset(insurance,filter == F)
insurance.train <- subset(insurance,filter == T)

# Q3

model.dt <- rpart(CarInsurance~., insurance.train)
#a
rpart.plot(model.dt,box.palette="RdBu", shadow.col="gray", nn=TRUE)
prediction.dt <- predict(model.dt, insurance.test)
actual.dt <- insurance.test$CarInsurance

#c
install.packages("randomForest") 
library(randomForest)
model.rf <- randomForest(CarInsurance~., insurance.train, importance = TRUE, na.action=na.exclude)

prediction.rf <- predict(model.rf, insurance.test)
actual.rf <- insurance.test$CarInsurance


conf.rf <- table(actual.rf,prediction.rf>0.6)

precision.rf <- conf.rf['1','TRUE'] / (conf.rf['1','TRUE'] + conf.rf['1', 'FALSE'])
recall.rf <-conf.rf['1','TRUE'] / (conf.rf['1','TRUE'] + conf.rf['0', 'TRUE'])

#d
library(pROC)

rocurve.dt <- roc(actual.dt, prediction.dt, direction = "<", levels = c("1", "0"))
rocurve.rf <- roc(actual.rf, prediction.rf, direction = "<", levels = c("1", "0"))

plot(rocurve.dt,col ='red', main = 'ROC Chart')
par(new = TRUE)
plot(rocurve.rf,col ='blue', main = 'ROC Chart')

# Q4
conf.rf <- table(actual.rf,prediction.rf>0.69)

precision.rf <- conf.rf['1', 'TRUE'] / (conf.rf['1', 'TRUE'] + conf.rf['1', 'FALSE'])
recall.rf <-conf.rf['1', 'TRUE'] / (conf.rf['1', 'TRUE'] + conf.rf['0', 'TRUE'])


varImpPlot(model.rf)






